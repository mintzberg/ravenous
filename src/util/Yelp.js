const clientId = 'GDnxaHEI3Yw0BY_oaUnWXw';
const apiKey =
  'siv4P6-fp-vqlhYODW2r79ulTacSXmEBDQO7rsBFJ4fb_CpSq91JFMHe4bQrTmi3cGJJGuZjFFDrgOK0i5KGMbJwCQAo1hkKHhVPSR4G8VN7IAQJHnugCAFAqp4bXHYx';

export const Yelp = {
  searchYelp(term, location, sortBy) {
    return fetch(
      `https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/businesses/search?term=${term}&location=${location}&sort_by=${sortBy}`,
      {
        headers: {
          Authorization: `Bearer ${apiKey}`
        }
      }
    )
      .then(response => {
        return response.json();
      })
      .then(jsonResponse => {
        if (jsonResponse.businesses) {
          return jsonResponse.businesses.map(business => {
            // return business
            return {
              id: business.id,
              imageSrc: business.image_url,
              name: business.name,
              address: business.location.address1,
              city: business.location.city,
              state: business.location.state,
              zipCode: business.location.zip_code,
              category: business.category,
              rating: business.rating,
              reviewCount: business.review_count
            };
          });
        }
      });
  }
};
